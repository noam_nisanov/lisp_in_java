package virtualMachine.lispProcedures;

import java.util.List;

import lispDataTypes.LispCommand;
import lispDataTypes.LispObject;
import lispDataTypes.LispProgram;
import lispDataTypes.LispString;
import lispDataTypes.LispVarPair;
import virtualMachine.LispEvaluator;
import virtualMachine.LispVarTable;

public class LispUserProcedure extends LispProcedure {
    private List<LispString> procedureArgs;
    private LispVarTable procedureFather;
    private LispProgram procedureBody;
    private int myId;
    private static int id=0;
    
    public LispUserProcedure(LispProgram procedureBody, List<LispString> procedureArguments, LispVarTable father) {
        myId = id;
        id++;
//        System.out.println("Adding proc: \n" + procedureBody.dumpProgram());
        this.procedureArgs = procedureArguments;
        this.procedureBody = procedureBody;
        this.procedureFather = father;
    }

    @Override
    public LispObject evalProcedure(LispCommand lispCmd, LispVarTable env) {
        // TODO apply parameters to the procedure
//        System.out.println("Call to user procedure!");
        if(procedureArgs.size() != lispCmd.getNumArgs() - 1) {
            // TODO throw exception here
            System.out.println("invalid amount of params passed to procedure!");
            System.out.println("Expected: " + procedureArgs.size());
            System.out.println("Recieved: " + (lispCmd.getNumArgs() - 1));
            int x = 0/0;
        }
        
        LispVarTable procVarTable = new LispVarTable(procedureFather);
        for(int i=1; i<lispCmd.getNumArgs(); i++) {
            LispObject evaledObj = LispEvaluator.eval(lispCmd.getArg(i), env);
            String objVarName = procedureArgs.get(i-1).getStringVal();
            LispVarPair varPair = new LispVarPair(objVarName, evaledObj);
            procVarTable.addVar(varPair);
        }
        
        return LispEvaluator.evalSerial(procedureBody, procVarTable);
    }


    @Override
    public String getStringVal() {
//        return "\n----------------\nLambdaProc:\n"+procedureBody.dumpProgram()+"----------------";
        return "Lisp_Lambda"+myId;
    }

}
