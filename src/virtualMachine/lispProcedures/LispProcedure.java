package virtualMachine.lispProcedures;

import java.io.IOException;

import lispDataTypes.LispCommand;
import lispDataTypes.LispObject;
import virtualMachine.LispVarTable;

public abstract class LispProcedure implements LispObject {

    public abstract LispObject evalProcedure(LispCommand lispCmd, LispVarTable env);


    protected boolean isParamsValid(LispCommand callCmd, int expectedParams, String errorMsg) {
        if (callCmd.getNumArgs() - 1 == expectedParams) {
            return true;
        }
        System.out.println(errorMsg);
        // TODO decide what to do here instead of collapsing
        int x = 0/0;
        return false;
    }

}
