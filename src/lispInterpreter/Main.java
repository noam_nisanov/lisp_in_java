package lispInterpreter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lispDataTypes.LispCommand;
import lispDataTypes.LispObject;
import lispDataTypes.LispProgram;
import lispDataTypes.LispString;
import lispDataTypes.lispNumbers.LispInt;
import virtualMachine.LispVM;
import virtualMachine.primitives.LispPrimitiveProcedure;

public class Main {

    public static void main(String[] args) throws IOException {
//        System.out.println("Udir: " + System.getProperty("user.dir"));
//        LispProgram mainProgram = getMainProgram();
//        mainProgram2.dumpProgram();
//        String sourceName = "test01.lsp";
        if(args.length == 0) {
            System.out.println("must give file name!");
            System.exit(1);
        } else if(args[0].equals("--help") || args[0].equals("-h")) {
            System.out.println(LispPrimitiveProcedure.dumpCommands());
            System.exit(2);
        }
        
        String sourceName = args[0];
        LispProgram mainProgram = LispCompiler.compile(System.getProperty("user.dir") + "/" + sourceName);
//        System.out.println("SSS");
//        mainProgram.dumpProgram();
//        lispVM.loadProgram(mainProgram);
        LispVM lispVM = new LispVM(mainProgram);
        lispVM.run();
    }


    private static LispProgram getMainProgram() {
        // (define x 3)
        List<LispObject> firstCmdArgs = new ArrayList<>();
        firstCmdArgs.add(new LispString("define"));
        firstCmdArgs.add(new LispString("x"));
        firstCmdArgs.add(new LispInt(3));
        LispCommand firstCmd = new LispCommand(firstCmdArgs);

        // (print 'x val = ')
        List<LispObject> secCmdArgs = new ArrayList<>();
        secCmdArgs.add(new LispString("print"));
        secCmdArgs.add(new LispString("'x val = '"));
        LispCommand secCmd = new LispCommand(secCmdArgs);

        // (print x)
        List<LispObject> thirdCmdArgs = new ArrayList<>();
        thirdCmdArgs.add(new LispString("print"));
        thirdCmdArgs.add(new LispString("x"));
        LispCommand thirdCmd = new LispCommand(thirdCmdArgs);
        
        // (define x2 (lambda (print 'x val = ')))
        List<LispObject> fourthCmdArgs = new ArrayList<>();
        fourthCmdArgs.add(new LispString("define"));
        fourthCmdArgs.add(new LispString("x2"));
        List<LispObject> fourthCmdLambda = new ArrayList<>();
        fourthCmdLambda.add(new LispString("lambda"));
        fourthCmdLambda.add(secCmd);
        fourthCmdArgs.add(new LispCommand(fourthCmdLambda));
        LispCommand fourthCmd = new LispCommand(fourthCmdArgs);
        
        // (x2)
        List<LispObject> fifthCmdArgs = new ArrayList<>();
//        fifthCmdArgs.add(new LispString("print"));
        fifthCmdArgs.add(new LispString("x2"));
        LispCommand fifthCmd = new LispCommand(fifthCmdArgs);

        
        List<LispObject> cmds = new ArrayList<>();
        cmds.add(firstCmd);
        cmds.add(secCmd);
        cmds.add(thirdCmd);
        cmds.add(fourthCmd);
        cmds.add(fifthCmd);

        LispProgram program = new LispProgram(cmds);

        return program;
    }

}
