package lispDataTypes;

public class LispString implements LispObject {
    
    private String value;
    
    public LispString(String string) {
        setValue(string);
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    public String getStringVal() {
//        System.out.println("returning: " +value);
        if(value.charAt(0) == '\'' && value.charAt(value.length()-1) == '\'') {
            return value.substring(1, value.length()-1);
        }
        return value;
//        return "|"+value+"|";
    }
}
